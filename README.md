| CircleCi | AppVeyor | NuGet | MyGet |  
| -- | -- | -- | -- |  
| [![CircleCI](https://circleci.com/bb/gfoidl/fastreflection/tree/master.svg?style=svg)](https://circleci.com/bb/gfoidl/fastreflection/tree/master) | [![Build status](https://ci.appveyor.com/api/projects/status/2d3c579a7gwpda23/branch/master?svg=true)](https://ci.appveyor.com/project/GntherFoidl/fastreflection/branch/master) | [![NuGet](https://img.shields.io/nuget/v/gfoidl.FastReflection.svg?style=flat-square)](https://www.nuget.org/packages/gfoidl.FastReflection/) | [![MyGet](https://img.shields.io/myget/gfoidl/v/gfoidl.FastReflection.svg?style=flat-square)](https://www.myget.org/feed/gfoidl/package/nuget/gfoidl.FastReflection) |  

# gfoidl.FastReflection
_gfoidl.FastReflection_ is a helper for boosting execution speed for [.net Reflection](https://msdn.microsoft.com/en-us/library/ms173183.aspx).  It is a .net Standard 2.0 library.

Usually .net Reflection is slow, because it is done at runtime by inspecting the meta-data of the assembly / types.  
_gfoidl.FastReflection_ significantly boosts execution speed for repetitive access to the same members. Not only are these member-accesses cached, but also there's a _delegate_ generated for accessing the member. Thus __allowing to use reflection at the speed of an delegate-call__.  

## How it's done?  
_gfoidl.FastReflection_ creates a _dynamic method_ via IL-code generation and then hands you out a delegate that your code can use.  
Because the dynamic method and the delegate needs to be generated, furthermore the dynamic methods needs to be JITted, the performance boost isn't available at the first call to _gfoidl.FastReflection_. All further calls for the same member, not necessarily on the same object -- but an object of the same type -- will get the benefit of this library.

## What is supported?  
_gfoidl.FastReflection_ boosts the following:  

* property getters  
* property setters  
* method-invocations (both static and instance methods)  

The types used can be __value- or reference-types__.
 
## Installation  
* grab the assembly [gfoidl.FastReflection.dll](https://bitbucket.org/gfoidl/fastreflection/downloads/gfoidl.FastReflection.dll) and add it to your project, or  
* install it from a [NuGet-package](https://www.nuget.org/packages/gfoidl.FastReflection)  
 
## How it is used?  
Suppose we have the following object:  
```
#!c#
public class Person
{
    public string FirstName { get; set; }
    public string LastName  { get; set; }

    public string GetFullName(string title)
    {
        return string.Format("{0} {1} {2}", title, this.FirstName, this.LastName);
    }
}
```

### Setting a property by _FastReflection_  
For settings a __property__ a delegate of type `FastSetter` is generated.
```
#!c#
Person person = new Person();

FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(Person), "FirstName");
setter(person, "Günther");

Debug.Assert(person.FirstName == "Günther");
```

### Reading a property by _FastReflection_
To read a __property__ a delegate of type `FastGetter` is generated.
```
#!c#
Person person   = new Person();
person.LastName = "Foidl";

FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(Person), "LastName");
string lastName   = getter(person) as string;

Debug.Assert(lastName == "Foidl");
```

### Calling a method by _FastReflection_  
To call a __method__ a delegate of type `FastMethod` is generated.
```
#!c#
Person person = new Person
{
    FirstName = "Günther",
    LastName  = "Foidl"
};

FastMethod method = FastReflectionHelper.GetFastMethod(typeof(Person), "GetFullName");
string fullName   = method(person, "Mr") as string;

Debug.Assert(fullName == "Mr Günther Foidl");
```  

-----
## There's a bug?  
I can't imagine that there might be a bug, but if you are sure so please [create an issue](https://bitbucket.org/gfoidl/fastreflection/issues). Be sure to check existing issues before filing a new one.  

## How to contribute  

1. create a fork  
2. make you edits / improvements in a specific branch  
3. make tests -- [NUnit](http://nunit.org/) is used as testing-framework  
4. merge the branch to (your) master-branch  
5. create a pull-request to the [main-repo](https://bitbucket.org/gfoidl/fastreflection)
 