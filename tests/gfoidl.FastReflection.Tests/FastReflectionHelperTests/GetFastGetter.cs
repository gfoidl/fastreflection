﻿using System;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    [TestFixture]
    public class GetFastGetter
    {
        [Test]
        public void Type_is_null___Throws_ArgumentNull()
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastGetter(null, "Name"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void PropertyName_is_null_or_emtpy___Throws_ArgumentNull([Values(null, "", " ")]string propertyName)
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastGetter(typeof(TestObject), propertyName));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ReferenceType_Property___Returns_correct_Delegate()
        {
            FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(TestObject), "Name");
            TestObject obj    = new TestObject();
            obj.Name          = "Anton";

            object actual = getter(obj);

            Assert.AreEqual("Anton", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ValueType_Property___Returns_correct_Delegate()
        {
            FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(TestObject), "Age");
            TestObject obj    = new TestObject();
            obj.Age           = 42;

            object actual = getter(obj);

            Assert.AreEqual(42, actual);
        }
    }
}
