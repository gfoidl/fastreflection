﻿using System;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    public class GetFastSetter_T
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            FastReflectionHelper.ClearCaches();
        }
        //---------------------------------------------------------------------
        [Test]
        public void Type_is_null___Throws_ArgumentNull()
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastSetter<int>(null, "Name"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void PropertyName_is_null_or_emtpy___Throws_ArgumentNull([Values(null, "", " ")]string propertyName)
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastSetter<int>(typeof(TestObject), propertyName));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ReferenceType_Property___throws_InvalidOperation()
        {
            Assert.Throws<InvalidOperationException>(() => FastReflectionHelper.GetFastSetter<int>(typeof(TestObject), "Name"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ValueType_Property___Returns_correct_Delegate()
        {
            FastSetter<int> setter = FastReflectionHelper.GetFastSetter<int>(typeof(TestObject), "Age");
            TestObject obj         = new TestObject();

            setter(obj, 30);

            Assert.AreEqual(30, obj.Age);
        }
    }
}
