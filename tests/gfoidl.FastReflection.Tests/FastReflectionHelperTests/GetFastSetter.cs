﻿using System;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    [TestFixture]
    public class GetFastSetter
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            FastReflectionHelper.ClearCaches();
        }
        //---------------------------------------------------------------------
        [Test]
        public void Type_is_null___Throws_ArgumentNull()
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastSetter(null, "Name"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void PropertyName_is_null_or_emtpy___Throws_ArgumentNull([Values(null, "", " ")]string propertyName)
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastSetter(typeof(TestObject), propertyName));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ReferenceType_Property___Returns_correct_Delegate()
        {
            FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(TestObject), "Name");
            TestObject obj    = new TestObject();

            setter(obj, "Gü");

            Assert.AreEqual("Gü", obj.Name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ValueType_Property___Returns_correct_Delegate()
        {
            FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(TestObject), "Age");
            TestObject obj    = new TestObject();

            setter(obj, 30);

            Assert.AreEqual(30, obj.Age);
        }
    }
}
