﻿using System;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    [TestFixture]
    public class GetFastMethod
    {
        [Test]
        public void Type_is_null___Throws_ArgumentNull()
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastMethod(null, "DoubleName"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Method_name_is_null_or_empty___Throws_ArgumentNull([Values(null, "", " ")]string methodName)
        {
            Assert.Throws<ArgumentNullException>(() => FastReflectionHelper.GetFastMethod(typeof(TestObject), methodName));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Void_no_args___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "DoubleName");
            TestObject obj    = new TestObject { Name = "abc" };

            method(obj);

            Assert.AreEqual("abcabc", obj.Name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Void_ReferenceType_Arg___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "SetName");
            TestObject obj    = new TestObject();

            method(obj, "Himen");

            Assert.AreEqual("Himen", obj.Name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Void_ValueType_Arg___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "SetAge");
            TestObject obj    = new TestObject();

            method(obj, 42);

            Assert.AreEqual(42, obj.Age);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Returns_ReferenceType_NoArgs___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetDoubledName");
            TestObject obj    = new TestObject { Name = "xyz" };

            string actual = method(obj) as string;

            Assert.AreEqual("xyzxyz", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Returns_ValueType_no_Args___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetDoubledAge");
            TestObject obj    = new TestObject { Age = 42 };

            int actual = (int)method(obj);

            Assert.AreEqual(84, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Returns_ReferenceType_ReferenceType_Arg___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetConcatenatedName");
            TestObject obj    = new TestObject { Name = "abc" };

            string actual = method(obj, "xyz") as string;

            Assert.AreEqual("abcxyz", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Returns_ValueType_ValueType_Arg___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetSummedAge");
            TestObject obj    = new TestObject { Age = 100 };

            int actual = (int)method(obj, 42);

            Assert.AreEqual(142, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Static_Method___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "ToUpper");

            string actual = method(null, "abcd") as string;

            Assert.AreEqual("ABCD", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Method_overload_1___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "Foo", typeof(int));
            TestObject obj    = new TestObject { Age = 100 };

            string actual = method(obj, 42) as string;

            Assert.AreEqual("42", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Method_overload_2___Returns_correct_Delegate()
        {
            FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "Foo", typeof(int), typeof(string));
            TestObject obj    = new TestObject { Age = 100 };

            string actual = method(obj, 42, "foo_") as string;

            Assert.AreEqual("foo_42", actual);
        }
    }
}
