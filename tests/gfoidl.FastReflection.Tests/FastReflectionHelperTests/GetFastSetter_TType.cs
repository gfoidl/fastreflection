﻿using System;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    [TestFixture]
    public class GetFastSetter_TType
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            FastReflectionHelper.ClearCaches();
        }
        //---------------------------------------------------------------------
        [Test]
        public void Reference_Type___correct_delegate()
        {
            Delegate actual = FastReflectionHelper.GetFastSetter<TestObject>("Name");

            Assert.IsInstanceOf<FastSetter>(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Value_Type___correct_delegate()
        {
            Delegate actual = FastReflectionHelper.GetFastSetter<TestObject>("Age");

            Assert.IsInstanceOf<FastSetter<int>>(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Nullable___correct_delegate()
        {
            Delegate actual = FastReflectionHelper.GetFastSetter<TestObject2>("Age");

            Assert.IsInstanceOf<FastSetter>(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void TestObject___OK()
        {
            FastSetter nameSetter     = FastReflectionHelper.GetFastSetter<TestObject>("Name") as FastSetter;
            FastSetter<int> ageSetter = FastReflectionHelper.GetFastSetter<TestObject>("Age") as FastSetter<int>;
            TestObject obj            = new TestObject();

            nameSetter(obj, "abc");
            ageSetter(obj, 42);

            Assert.AreEqual("abc", obj.Name);
            Assert.AreEqual(42, obj.Age);
        }
        //---------------------------------------------------------------------
        [Test]
        public void TestObject2___OK()
        {
            FastSetter nameSetter = FastReflectionHelper.GetFastSetter<TestObject2>("Name") as FastSetter;
            FastSetter ageSetter  = FastReflectionHelper.GetFastSetter<TestObject2>("Age") as FastSetter;
            TestObject2 obj       = new TestObject2();

            nameSetter(obj, "abc");
            ageSetter(obj, 42);

            Assert.AreEqual("abc", obj.Name);
            Assert.AreEqual(42, obj.Age);
        }
    }
}
