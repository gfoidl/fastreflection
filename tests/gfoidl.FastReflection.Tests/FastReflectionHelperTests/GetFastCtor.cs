﻿using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.FastReflectionHelperTests
{
    [TestFixture]
    public class GetFastCtor
    {
        [Test]
        public void Correct_type_created()
        {
            FastCtor<TestObject> ctor = FastReflectionHelper.GetFastCtor<TestObject>();

            TestObject actual = ctor();

            Assert.IsInstanceOf<TestObject>(actual);
        }
    }
}
