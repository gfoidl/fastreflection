﻿namespace gfoidl.FastReflection.Tests
{
    public class TestObject
    {
        public string Name { get; set; }
        public int Age     { get; set; }
        //---------------------------------------------------------------------
        public void DoubleName()
        {
            this.Name += this.Name;
        }
        //---------------------------------------------------------------------
        public void SetName(string newName)
        {
            this.Name = newName;
        }
        //---------------------------------------------------------------------
        public void SetAge(int newAge)
        {
            this.Age = newAge;
        }
        //---------------------------------------------------------------------
        public string GetDoubledName()
        {
            return this.Name + this.Name;
        }
        //---------------------------------------------------------------------
        public int GetDoubledAge()
        {
            return this.Age + this.Age;
        }
        //---------------------------------------------------------------------
        public string GetConcatenatedName(string text)
        {
            return this.Name + text;
        }
        //---------------------------------------------------------------------
        public int GetSummedAge(int summand)
        {
            return this.Age + summand;
        }
        //---------------------------------------------------------------------
        public static string ToUpper(string input)
        {
            return input.ToUpper();
        }
        //---------------------------------------------------------------------
        public string Foo(int num)
        {
            return num.ToString();
        }
        //---------------------------------------------------------------------
        public string Foo(int num, string prefix)
        {
            return prefix + num.ToString();
        }
    }
    //-------------------------------------------------------------------------
    public class TestObject2
    {
        public string Name { get; set; }
        public int? Age    { get; set; }
    }
}
