﻿using System.IO;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.KeyTests
{
    [TestFixture]
    public class Equals
    {
        [Test]
        public void Object_null___Returns_false()
        {
            var sut = new Key(typeof(MemoryStream), "Read");

            Assert.IsFalse(sut.Equals(null));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Object_of_different_Type___Returns_false()
        {
            var sut    = new Key(typeof(MemoryStream), "Read");
            object obj = new object();

            Assert.IsFalse(sut.Equals(obj));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Object_of_same_Instance___Returns_true()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            object obj = sut;

            Assert.IsTrue(sut.Equals(obj));
        }
        //---------------------------------------------------------------------
        [Test]
        public void With_it_self___Returns_true()
        {
            var sut = new Key(typeof(MemoryStream), "Read");

            Assert.IsTrue(sut.Equals(sut));
        }
        //---------------------------------------------------------------------
        [Test]
        public void With_coyp_of_it_self___Returns_true()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(MemoryStream), "Read");

            Assert.IsTrue(sut.Equals(key));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Different_Type___Returns_false()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(FileStream), "Read");

            Assert.IsFalse(sut.Equals(key));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Different_MemberName___Returns_false()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(MemoryStream), "Write");

            Assert.IsFalse(sut.Equals(key));
        }
        //---------------------------------------------------------------------
        [Test]
        public void This_has_ArgumentTypes_other_not___Returns_false()
        {
            var sut = new Key(typeof(MemoryStream), "Read", typeof(int));
            var key = new Key(typeof(MemoryStream), "Read");

            Assert.IsFalse(sut.Equals(key));
        }
        //---------------------------------------------------------------------
        [Test]
        public void This_and_other_has_ArgumentTypes___Returns_true()
        {
            var sut = new Key(typeof(MemoryStream), "Read", typeof(int), typeof(string));
            var key = new Key(typeof(MemoryStream), "Read", typeof(int), typeof(string));

            Assert.IsTrue(sut.Equals(key));
        }
    }
}
