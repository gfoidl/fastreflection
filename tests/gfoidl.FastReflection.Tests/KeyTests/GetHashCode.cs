﻿using System.IO;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.KeyTests
{
    [TestFixture]
    public class GetHashCode
    {
        [Test]
        public void With_it_self___Returns_same_HashCode()
        {
            var sut = new Key(typeof(MemoryStream), "Read");

            Assert.AreEqual(sut.GetHashCode(), sut.GetHashCode());
        }
        //---------------------------------------------------------------------
        [Test]
        public void With_copy_of_it_self___Returns_same_HashCode()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(MemoryStream), "Read");

            Assert.AreEqual(sut.GetHashCode(), key.GetHashCode());
        }
        //---------------------------------------------------------------------
        [Test]
        public void Different_Type___Returns_different_HashCode()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(FileStream), "Read");

            Assert.AreNotEqual(sut.GetHashCode(), key.GetHashCode());
        }
        //---------------------------------------------------------------------
        [Test]
        public void Different_MemberName___Returns_different_HashCode()
        {
            var sut = new Key(typeof(MemoryStream), "Read");
            var key = new Key(typeof(MemoryStream), "Write");

            Assert.AreNotEqual(sut.GetHashCode(), key.GetHashCode());
        }
        //---------------------------------------------------------------------
        [Test]
        public void This_and_other_has_ArgumentTypes___Returns_same_HashCode()
        {
            var sut = new Key(typeof(MemoryStream), "Read", typeof(int), typeof(string));
            var key = new Key(typeof(MemoryStream), "Read", typeof(int), typeof(string));

            Assert.AreEqual(sut.GetHashCode(), key.GetHashCode());
        }
    }
}
