﻿using System.IO;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests.KeyTests
{
    [TestFixture]
    public class Ctor
    {
        [Test]
        public void Args_given___Correct_Properties_set()
        {
            var sut = new Key(typeof(MemoryStream), "Read");

            Assert.AreEqual(typeof(MemoryStream), sut.Type);
            Assert.AreEqual("Read", sut.MemberName);
        }
    }
}
