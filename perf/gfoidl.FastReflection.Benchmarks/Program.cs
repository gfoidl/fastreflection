﻿using BenchmarkDotNet.Running;

namespace gfoidl.FastReflection.Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<CtorBenchmarks>();
        }
    }
}
