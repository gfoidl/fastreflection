﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Columns;
using BenchmarkDotNet.Attributes.Jobs;

namespace gfoidl.FastReflection.Benchmarks
{
    [ClrJob, CoreJob]
    [RankColumn]
    public class CtorBenchmarks
    {
        private static readonly Func<TestObject>     _del = () => new TestObject();
        private static readonly FastCtor<TestObject> _fastCtor = FastReflectionHelper.GetFastCtor<TestObject>();
        //---------------------------------------------------------------------
        [Benchmark(Baseline = true)]
        public TestObject Ctor()
        {
            return new TestObject();
        }
        //---------------------------------------------------------------------
        [Benchmark]
        public TestObject Delegate()
        {
            return _del();
        }
        //---------------------------------------------------------------------
        [Benchmark]
        public TestObject Generic_new_T()
        {
            return Create<TestObject>();
        }
        //---------------------------------------------------------------------
        [Benchmark]
        public TestObject FastCtor()
        {
            return FastReflectionHelper.GetFastCtor<TestObject>()();
        }
        //---------------------------------------------------------------------
        [Benchmark]
        public TestObject FastCtorCached()
        {
            return _fastCtor();
        }
        //---------------------------------------------------------------------
        private static T Create<T>() where T : class, new() => new T();
    }
}
