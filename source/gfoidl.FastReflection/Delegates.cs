﻿namespace gfoidl.FastReflection
{
    public delegate T      FastCtor<T>() where T : class, new();
    public delegate object FastMethod(object instance, params object[] args);
    public delegate void   FastSetter(object target, object value);
    public delegate void   FastSetter<T>(object target, T value) where T : struct;
    public delegate object FastGetter(object target);
}
