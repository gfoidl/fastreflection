﻿using System;
using System.Diagnostics;

namespace gfoidl.FastReflection
{
    [DebuggerNonUserCode]
    internal readonly struct Key : IEquatable<Key>
    {
        public Type     Type          { get; }
        public string   MemberName    { get; }
        public Type[]   ArgumentTypes { get; }
        //---------------------------------------------------------------------
        public Key(Type type, string memberName, params Type[] argumentTypes) : this()
        {
            this.Type          = type;
            this.MemberName    = memberName;
            this.ArgumentTypes = argumentTypes;
        }
        //---------------------------------------------------------------------
        #region IEquatable<Key> Members
        public bool Equals(Key other)
        {
            bool isEqual = this.Type == other.Type && this.MemberName == other.MemberName;

            if (!isEqual) return false;
            if (this.ArgumentTypes.Length != other.ArgumentTypes.Length) return false;

            for (int i = 0; i < this.ArgumentTypes.Length; ++i)
                if (this.ArgumentTypes[i] != other.ArgumentTypes[i]) return false;

            return true;
        }
        //---------------------------------------------------------------------
        public override bool Equals(object obj) => obj is Key other && this.Equals(other);
        //---------------------------------------------------------------------
        public override int GetHashCode()
        {
            // https://stackoverflow.com/a/263416/347870
            unchecked
            {
                int hash = 17;
                hash     = hash * 23 + this.Type.GetHashCode();
                hash     = hash * 23 + this.MemberName.GetHashCode();

                return hash;
            }
        }
        #endregion
    }
}
