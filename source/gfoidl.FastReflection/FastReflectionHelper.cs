﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reflection;

namespace gfoidl.FastReflection
{
#if !DEBUG
    [DebuggerNonUserCode]
#endif
    public static class FastReflectionHelper
    {
        private static ConcurrentDictionary<Type, Delegate>  _ctorCache   = new ConcurrentDictionary<Type, Delegate>();
        private static ConcurrentDictionary<Key, FastMethod> _methodCache = new ConcurrentDictionary<Key, FastMethod>();
        private static ConcurrentDictionary<Key, Delegate>   _setterCache = new ConcurrentDictionary<Key, Delegate>();
        private static ConcurrentDictionary<Key, FastGetter> _getterCache = new ConcurrentDictionary<Key, FastGetter>();
        //---------------------------------------------------------------------
        public static FastCtor<T> GetFastCtor<T>() where T : class, new()
        {
            Type t       = typeof(T);
            Delegate del = _ctorCache.GetOrAdd(t, DynamicMethodHelper.CreateFastCtor<T>);

            return del as FastCtor<T>;
        }
        //---------------------------------------------------------------------
        public static FastMethod GetFastMethod(Type target, string methodName, params Type[] types)
        {
            Key key = GetKey(target, methodName, types);
            return _methodCache.GetOrAdd(key, DynamicMethodHelper.CreateFastMethod);
        }
        //---------------------------------------------------------------------
        public static FastSetter GetFastSetter(Type target, string propertyName)
        {
            Key key      = GetKey(target, propertyName);
            Delegate del = _setterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastSetter);

            return del as FastSetter;
        }
        //---------------------------------------------------------------------
        public static FastSetter<T> GetFastSetter<T>(Type target, string propertyName) where T : struct
        {
            Key key      = GetKey(target, propertyName);
            Delegate del = _setterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastSetter<T>);

            return del as FastSetter<T>;
        }
        //---------------------------------------------------------------------
        public static Delegate GetFastSetter<TType>(string propertyName)
        {
            Key key = GetKey(typeof(TType), propertyName);
            return _setterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastSetter2);
        }
        //---------------------------------------------------------------------
        public static FastGetter GetFastGetter(Type target, string propertyName)
        {
            Key key = GetKey(target, propertyName);
            return _getterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastGetter);
        }
        //---------------------------------------------------------------------
        private static Key GetKey(Type target, string memberName, params Type[] types)
        {
            if (target == null)                        ThrowHelper.ThrowArgumentNullException(ThrowHelper.ExceptionArgument.target);
            if (string.IsNullOrWhiteSpace(memberName)) ThrowHelper.ThrowArgumentNullException(ThrowHelper.ExceptionArgument.memberName);

            return new Key(target, memberName, types);
        }
        //---------------------------------------------------------------------
        // For Testing:
        internal static void ClearCaches()
        {
            _ctorCache.Clear();
            _setterCache.Clear();
            _getterCache.Clear();
            _methodCache.Clear();
        }
    }
}
