﻿using System;

namespace gfoidl.FastReflection
{
    internal static class ThrowHelper
    {
        public static void ThrowArgumentNullException(ExceptionArgument exceptionArgument)
            => throw GetArgumentNullException(exceptionArgument);
        //---------------------------------------------------------------------
        private static Exception GetArgumentNullException(ExceptionArgument exceptionArgument)
            => new ArgumentNullException(exceptionArgument.ToString());
        //---------------------------------------------------------------------
        public enum ExceptionArgument
        {
            memberName,
            target
        }
    }
}
